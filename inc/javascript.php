	<footer class="main-footer">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<small class="copyright">
						Copyright &copy; <?php echo date('Y'); ?> <a href="#www">www</a> All Rights Reserved.
					</small>
				</div>
			</div>
		</div>
	</footer>
</div>
<script src="assets/js/core/jquery.easing.js"></script>
<script src="assets/js/core/jquery.mousewheel.js"></script>
<script src="assets/js/core/detect.min.js"></script>
<script src="assets/js/vendor/fancybox/jquery.fancybox.min.js"></script>
<script src="assets/js/vendor/slick/slick.min.js"></script>
<!-- croppie -->
<link rel="stylesheet" href="assets/js/vendor/croppie/croppie.css">
<script src="assets/js/vendor/croppie/exif.min.js"></script>
<script src="assets/js/vendor/croppie/croppie.min.js"></script>
<!-- main js -->
<script src="assets/js/main.js<?php echo $site['cache_version']; ?>"></script>