<?php include('inc/head.php'); ?>
	<?php include('inc/header.php'); ?>
	<main class="main-container" role="main">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<section class="home">
						<div class="herobanner">
							<img class="img bg" src="assets/img/herobanner/bg.png" alt="bg">
							<img class="img tire" src="assets/img/herobanner/tire.png" alt="tire">
							<img class="img line-bottom" src="assets/img/herobanner/line-bottom.png" alt="line bottom">
							<img class="img line-top" src="assets/img/herobanner/line-top.png" alt="line top">
							<img class="img text" src="assets/img/herobanner/text.png" alt="text">
							<img class="img logo" src="assets/img/herobanner/logo.png" alt="logo">
						</div>
						<div class="box-btn">
							<p class="btn-1">
								<a href="select-activity.php"><img src="assets/img/btn_fb-login.png" alt="LOGIN WITH FACEBOOK"></a>
							</p>
							<p class="btn-2">
								<a href="#"><img src="assets/img/btn_rules.png" alt="กติการ่วมสนุก"></a>
							</p>
						</div>
					</section>
				</div>
			</div>
		</div>
	</main>
<?php include('inc/javascript.php'); ?>
<?php include('inc/footer.php'); ?>