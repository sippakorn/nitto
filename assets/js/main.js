/* =================================
	VARIABLE & CALL
================================= */
var $app = {};
var user = detect.parse(navigator.userAgent);
var is
$(document).ready(function(){
	$app.check.init();
	$app.global.init();
});
$(window).on('load',function(){
	$('html').addClass('isLoad');
});

/* =================================
	FUNCTION
================================= */
$app.check = {
	init : function(){
	},
	/* CHECK IE */
	ie : function(){
		if(user.browser.family == "IE" && user.browser.major < 11){
			return true;
		} else {
			return false;
		}
	},
	/* CHECK SAFARI */
	safari : function(){
		if(user.device.type == "Desktop" && user.browser.family == "Safari" && user.browser.major < 12){
			return true;
		} else {
			return false;
		}
	},
}
$app.global = {
	init : function(){
		this.menu();
		this.tab();
		this.selectbox();
		this.accordion();
	},
	menu : function(){
		var isShowMenu = false;
		$(".btn-menu").click(function(e) {
			if(isShowMenu) {
				$('html').removeClass('show-menu');
				isShowMenu = false;
			} else {
				$('html').addClass('show-menu');
				isShowMenu = true;
			}
			e.preventDefault();
		});
	},
	tab : function(){
		var now_tab = 0;
		if($('.tabbox').length>0){
			$('.tabbox').each(function(){
				$(this).find('.tb-select li').removeClass('active');
				$(this).find('.tb-item').removeClass('active').hide();
				$(this).find('.tb-select li').eq(now_tab).addClass('active');
				$(this).find('.tb-item').eq(now_tab).addClass('active').show();
			});
			$('.tb-select li a').click(function(e) {
				now_tab = $(this).parent().index();
				$(this).parents('.tabbox').find('.tb-select li').removeClass('active');
				$(this).parents('.tabbox').find('.tb-select li').eq(now_tab).addClass('active');
				$(this).parents('.tabbox').find('.tb-item').removeClass('active').hide();
				$(this).parents('.tabbox').find('.tb-item').eq(now_tab).addClass('active').show();
				e.preventDefault();
			});
		}
	},
	selectbox : function(){
		if($('.selectbox').length>0){
			$('html').click(function(){
				$('.selectbox').removeClass('show-options');
			});
			$('.selectbox .sb-selector').click(function(e){
				if($(this).parent('.selectbox').hasClass('show-options')){
					$(this).parent('.selectbox').removeClass('show-options');
				} else {
					$(this).parent('.selectbox').addClass('show-options');
				}
				e.stopPropagation();
			});
			$('.selectbox .sb-options a').click(function(e){
				var val = $(this).attr('data-value');
				var txt = $(this).text();
				$(this).parents('.selectbox').find('.sb-selector').text(txt).addClass('selected');
				$(this).parents('.selectbox').removeClass('show-options');
				$(this).parents('.selectbox').find('[name="sb-value"]').val(val);
				e.preventDefault();
			});
		}
	},
	accordion : function(){
		if($('.accordionbox').length>0){
			$('.accordionbox .ab-label').click(function(e){
				$(this).parent().siblings('.active').removeClass('active').find('.ab-content').slideUp();;
				if($(this).parent().hasClass('active')){
					$(this).parent().removeClass('active').find('.ab-content').slideUp();
				} else {
					$(this).parent().addClass('active').find('.ab-content').slideDown();
				}
				e.preventDefault();
			});
		}
	},
}