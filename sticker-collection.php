<?php include('inc/head.php'); ?>
	<?php include('inc/header.php'); ?>
	<main class="main-container" role="main">
		<section class="sticker-collection">
			<header class="header-page">
				<br>
				<h1 class="head text-center">
					<strong>สติ๊กเกอร์ของฉัน</strong><br><small>ประวัติการดาวน์โหลดสติ๊กเกอร์</small>
				</h1>
				<img src="assets/img/line-red.png" alt="">
				<br>
			</header>
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-10">
						<?php for ($i=0; $i < 2 ; $i++) { ?>
						<div class="sticker-list">
							<div class="num"><?php echo $i+1; ?>.</div>
							<div class="row row-s">
								<div class="col-5">
									<a class="sticker-item" data-sticker="<?php echo $i+1; ?>" href="#">
										<img class="img" src="assets/img/sticker/1-thumb.png" alt="">
										<span class="box-txt">
											<strong>หมีโหด</strong> กระโดดกัด <small>ขนาด 240x240px</small>
										</span>
									</a>
								</div>
								<div class="col-7">
									<div class="box-txt">
										<p>
											สติ๊กเกอร์ : หมีโหด กระโดดกัด<br>
											วันที่สร้าง : 12/12/19 14.30 น.
										</p>
										<p>
											<a class="btn-detail" href="#sticker-detail-1" data-fancybox><img src="assets/img/btn_view-detail.png" alt="ดูรายละเอียด"></a>
										</p>
									</div>
								</div>
							</div>
							<div style="display:none;">
								<div class="popup popup-sticker-detail" id="sticker-detail-<?php echo $i; ?>">
									<div class="head-popup">รายละเอียดสติ๊กเกอร์</div>
									<div class="content-popup text-center">
										<div class="container-fluid">
											<div class="row">
												<div class="col-12">
													<div class="head-content">หมีโหด กระโดดกัด</div>
													<img class="color-item" src="assets/img/sticker/color/black.png" alt="">
													<p class="box-img">
														<img src="assets/img/_demo/example-result.png" alt="">
													</p>
													<div class="row row-s">
														<div class="col-6">
															<a href="#"><img src="assets/img/btn_save-img.png" alt="Save Image"></a>
														</div>
														<div class="col-6">
															<a href="#"><img src="assets/img/btn_share-fb.png" alt="Share to Facebook"></a>
														</div>
													</div>
													<div class="row row-s justify-content-center">
														<div class="col-7">
															<p>
																<a href="#"><img src="assets/img/btn_download.png" alt="Download File"></a>
															</p>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</section>
	</main>
<?php include('inc/javascript.php'); ?>
<?php include('inc/footer.php'); ?>