<header class="main-header">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<a class="main-logo" href="index.php"><img src="assets/img/nitto-logo.svg" alt="NITTO"></a>
				<a class="btn-menu" href="javascript:void(0);">
					<span class="hamburger"></span>
					<span class="close"></span>
				</a>
			</div>
		</div>
	</div>
	<nav class="main-nav" role="navigation">
		<div class="inner">
			<div class="mn-head media">
				<div class="media-body align-self-center">
					<p class="text-right m-0"><strong><small>ยินดีต้อนรับ</small></strong><br>NITTO TIRE</p>
				</div>
				<strong class="initial-name">N</strong>
			</div>
			<div class="mn-menu">
				<a class="item" href="index.php">หน้าแรก</a>
				<a class="item" href="#">กติการ่วมสนุก</a>
				<a class="item" href="#">ประกาศผลรางวัล</a>
				<a class="item" href="sticker-collection.php">สติ๊กเกอร์ของฉัน</a>
			</div>
		</div>
	</nav>
</header>