<?php include('inc/head.php'); ?>
	<?php include('inc/header.php'); ?>
	<main class="main-container" role="main">
		<section class="activity-1">
			<div class="step-select-sticker box-step" id="step-1">
				<div class="container-fluid p-0">
					<div class="row no-gutters">
						<div class="col-12">
							<header class="header-page">
								<h1 class="head"><img src="assets/img/txt_head-activity-1.png" alt="ลายสุดเฟี้ยว คันเดียวในโลก"></h1>
							</header>
							<br>
							<div class="step-1">
								<div class="header-section">
									<h2 class="head"><img src="assets/img/txt_head-step-1.png" alt="STEP 1 เลือก STICKER"></h2>
								</div>
								<div class="sticker-select">
									<div class="item">
										<a class="sticker-item" href="javascript:void(0);" data-color="black,red,gray">
											<img class="img" src="assets/img/sticker/1-thumb.png" alt="">
											<span class="box-txt">
												<strong>หมีโหด</strong> กระโดดกัด <small>ขนาด 240x240px</small>
											</span>
										</a>
									</div>
									<div class="item">
										<a class="sticker-item" href="javascript:void(0);" data-color="red,black">
											<img class="img" src="assets/img/sticker/2-thumb.png" alt="">
											<span class="box-txt">
												<strong>หมีโหด</strong> กระโดดกัด<small>ขนาด 240x240px</small>
											</span>
										</a>
									</div>
									<div class="item">
										<a class="sticker-item" href="javascript:void(0);" data-color="red,gray">
											<img class="img" src="assets/img/sticker/3-thumb.png" alt="">
											<span class="box-txt">
												<strong>หมีโหด</strong> กระโดดกัด<small>ขนาด 240x240px</small>
											</span>
										</a>
									</div>
									<div class="item">
										<a class="sticker-item" href="javascript:void(0);" data-color="black,red,gray">
											<img class="img" src="assets/img/sticker/1-thumb.png" alt="">
											<span class="box-txt">
												<strong>หมีโหด</strong> กระโดดกัด<small>ขนาด 240x240px</small>
											</span>
										</a>
									</div>
								</div>
							</div>
							<br>
							<div class="step-2">
								<div class="header-section">
									<h2 class="head"><img src="assets/img/txt_head-step-2.png" alt="STEP 2 Upload รูปรถของคุณ"></h2>
								</div>
								<div class="col">
									<div class="box-btn-upload row">
										<div class="col align-self-center">
											<a href="javascript:void(0);" class="d-block btn-upload" onclick="step2();"><img src="assets/img/btn_upload.png" alt="UPLOAD"/></a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="step-upload box-step" id="step-2" style="display: none;">
				<div class="container-fluid p-0">
					<div class="row no-gutters">
						<div class="col-12">
							<header class="header-page">
								<h1 class="head">
									<img src="assets/img/txt_head-upload.png" alt="Upload รูปรถของคุณ">
								</h1>
								<a class="btn-back" href="javascript:void(0);" onclick="step1();">< back</a>
								<a class="btn-help" href="#popup-howto" data-fancybox><img src="assets/img/btn_help.png" alt="Help"></a>
							</header>
							<!-- FRONT -->
							<div class="header-section">
								<h2 class="head"><img src="assets/img/txt_head-upload-1.png" alt="1. ด้านหน้า (Front)"></h2>
							</div>
							<div class="box-upload">
								<label class="label" for="sticker-front-upload">
									<img class="outline" src="assets/img/upload-outline-front.png" alt="">
									<div class="crop-img front-img-data"></div>
									<input class="input" type="file" accept="image/*" id="sticker-front-upload"/>
								</label>
								<span class="num">1</span>
							</div>
							<!-- / FRONT -->
							<!-- SIDE -->
							<div class="header-section">
								<h2 class="head"><img src="assets/img/txt_head-upload-2.png" alt="2. ด้านข้าง (SIDE)"></h2>
							</div>
							<div class="box-upload">
								<label class="label" for="sticker-side-upload">
									<img class="outline" src="assets/img/upload-outline-side.png" alt="">
									<div class="crop-img side-img-data"></div>
									<input class="input" type="file" accept="image/*" id="sticker-side-upload"/>
								</label>
								<span class="num">2</span>
							</div>
							<!-- / SIDE -->
							<!-- REAR -->
							<div class="header-section">
								<h2 class="head"><img src="assets/img/txt_head-upload-3.png" alt="3. ด้านท้าย (REAR)"></h2>
							</div>
							<div class="box-upload">
								<label class="label" for="sticker-rear-upload">
									<img class="outline" src="assets/img/upload-outline-rear.png" alt="">
									<div class="crop-img rear-img-data"></div>
									<input class="input" type="file" accept="image/*" id="sticker-rear-upload"/>
								</label>
								<span class="num">3</span>
							</div>
							<!-- / REAR -->
							<!-- BACK -->
							<div class="header-section">
								<h2 class="head"><img src="assets/img/txt_head-upload-4.png" alt="4. ด้านหลัง (BACK)"></h2>
							</div>
							<div class="box-upload">
								<label class="label" for="sticker-back-upload">
									<img class="outline" src="assets/img/upload-outline-back.png" alt="">
									<div class="crop-img back-img-data"></div>
									<input class="input" type="file" accept="image/*" id="sticker-back-upload"/>
								</label>
								<span class="num">4</span>
							</div>
							<!-- / BACK -->
							<p>
								<br>
								<a class="d-block" href="javascript:void(0);" onclick="step3();"><img src="assets/img/btn_next.png" alt="NEXT"></a>
							</p>
						</div>
					</div>
				</div>
			</div>
			<!-- SELECT COLOR -->
			<div class="step-color box-step" id="step-3" style="display: none;">
				<div class="container-fluid p-0">
					<div class="row no-gutters">
						<div class="col-12">
							<header class="header-page">
								<h1 class="head">
									<img src="assets/img/txt_head-select-color.png" alt="เลือกสีที่ต้องการ">
								</h1>
								<a class="btn-back" href="javascript:void(0);" onclick="step2();">< back</a>
							</header>
							<br>
						</div>
					</div>
				</div>
				<div class="container">
					<div class="row">
						<div class="col-12">
							<p class="box-select-color"></p>
							<div class="box-preview-color ex-result">
								<div class="item item-front">
									<img class="sticker front" src=""/>
									<div class="result front-img-data"></div>
									<div class="label">FRONT</div>
								</div>
								<div class="item item-side">
									<img class="sticker side" src=""/>
									<div class="result side-img-data"></div>
									<div class="label">SIDE</div>
								</div>
								<div class="item item-rear">
									<img class="sticker rear" src=""/>
									<div class="result rear-img-data"></div>
									<div class="label">REAR</div>
								</div>
								<div class="item item-back">
									<img class="sticker back" src=""/>
									<div class="result back-img-data"></div>
									<div class="label">BACk</div>
								</div>
							</div>
							<p>
								<br>
								<a class="d-block" href="javascript:void(0);" onclick="step4();"><img src="assets/img/btn_finish.png" alt="finish"></a>
							</p>
						</div>
					</div>
				</div>
			</div>
			<!-- / SELECT COLOR -->
			<!-- PREVIEW RESULT -->
			<div class="step-result box-step" id="step-4" style="display: none;">
				<div class="container-fluid p-0">
					<div class="row no-gutters">
						<div class="col-12">
							<p>
								<a class="btn-back" href="javascript:void(0);"><img src="assets/img/btn_back.png" alt="< BACK" onclick="step3();"></a>
							</p>
							<div class="header-section">
								<h2 class="head"><img src="assets/img/txt_head-step-3.png" alt="STEP 3 DOWNLOAD & SHARE"></h2>
							</div>
							<br>
						</div>
					</div>
				</div>
				<div class="container">
					<div class="row">
						<div class="col-12">
							<div class="box-preview-img ex-result">
								<div class="box-result" id="final-img"></div>
								<p class="text-right mt-2"><img class="color-item" src="" alt=""></p>
							</div>
							<p class="text-center">
								<small>image size : 1,444 x 724px<br>File size : 250 MB</small>
							</p>
							<div class="row row-s">
								<div class="col-6">
									<a href="#" id="btn-save"><img src="assets/img/btn_save-img.png" alt="Save Image"></a>
								</div>
								<div class="col-6">
									<a href="#"><img src="assets/img/btn_share-fb.png" alt="Share to Facebook"></a>
								</div>
							</div>
							<div class="row row-s justify-content-center">
								<div class="col-7">
									<p>
										<a href="#" id="btn-download"><img src="assets/img/btn_download.png" alt="Download File"></a>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- / PREVIEW RESULT -->
			<div style="display:none;">
				<div id="popup-howto">
					<img src="assets/img/popup-howto.jpg" alt="">
				</div>
				<?php
					$sticker = array(
						array('ด้านหน้า','front'),
						array('ด้านข้าง','side'),
						array('ด้านท้าย','rear'),
						array('ด้านหลัง','back')
					);
					foreach ($sticker as $key => $v) {
				 ?>
				<!-- POPUP <?php echo $v[1]; ?> CROP -->
				<div class="popup popup-crop-img" id="popup-<?php echo $v[1]; ?>-crop">
					<div class="head-popup">จัดรูปของคุณให้ตรงกับเส้นประ</div>
					<div class="content-popup text-center">
						<div class="head"><strong><?php echo ($key+1).'.'.$v[0]; ?></strong> (<?php echo $v[1]; ?>)</div>
						<div class="box-crop-container">
							<div class="box-crop">
								<div class="box-outline">
									<img class="outline" src="assets/img/upload-outline-<?php echo $v[1]; ?>.png" alt="">
									<img class="sticker" src="" alt="">
								</div>
								<div class="box-img" id="sticker-<?php echo $v[1]; ?>-crop"></div>
							</div>
							<div class="box-tools">
								<!-- <a class="item" href="javascript:void(0);"><img src="assets/img/icon/hand.png" alt="Move"></a> -->
								<a class="item btn-zoom-<?php echo $v[1]; ?>" href="javascript:void(0);" data-zoom="in"><img src="assets/img/icon/zoom-in.png" alt="Zoom In"></a>
								<a class="item btn-zoom-<?php echo $v[1]; ?>" href="javascript:void(0);" data-zoom="out"><img src="assets/img/icon/zoom-out.png" alt="Zoom Out"></a>
								<a class="item btn-flip" id="btn-flip-<?php echo $v[1]; ?>" href="javascript:void(0);"><img src="assets/img/icon/flip.png" alt="Flip"></a>
							</div>
							<br>
							<p>
								<a href="javascript:void(0);" id="btn-crop-<?php echo $v[1]; ?>"><img src="assets/img/btn_done.png" alt="DONE"></a>
							</p>
							<br>
						</div>
					</div>
				</div>
				<!-- / POPUP <?php echo $v[1]; ?> CROP -->
				<?php } ?>
			</div>
		</section>
		<div style="display: none;">
			<canvas id="result-canvas" width="1444" height="1084"></canvas>
		</div>
	</main>
<?php include('inc/javascript.php'); ?>
<script>
	var $stickerSlider = $('.sticker-select');
	var $sticker = {
		style : 1,
		color : [],
		colorSelect : 0,
		sizeWidth : 720,
		sizeHeight : 540,
		path : 'assets/img/sticker/'
	}
	$(document).ready(function(){
		$('html').addClass('activity-page');
		// SELECT STYLE
		$stickerSlider.on('init', function(event, slick){
			$sticker.style = 1;
			fn_selectSticker($sticker.style);
		});
		$stickerSlider.slick({
			centerMode: true,
			centerPadding: 0,
			slidesToShow: 3
		});
		$stickerSlider.on('afterChange', function(event, slick, currentSlide, nextSlide){
		  $sticker.style = currentSlide+1;
		  fn_selectSticker($sticker.style);
		});
		// sticker init
		$stickerFront.init();
		$stickerSide.init();
		$stickerRear.init();
		$stickerBack.init();
	});
	function step1(){
		$(".box-step").hide();
		$("#step-1").show();
		$stickerSlider.slick('setPosition');
	}
	function step2(){
		$(".box-step").hide();
		$("#step-2").show();
	}
	function step3(){
		if($stickerFront.data != null && $stickerSide.data != null && $stickerRear.data != null && $stickerBack.data != null){
			$(".box-step").hide();
			$("#step-3").show();
		} else {
			alert('กรุณาเลือกรูปให้ครบทุกตำแหน่ง');
		}
	}
	function step4(){
		fn_loading();
		fn_createImg($stickerFront.data,$stickerSide.data,$stickerRear.data,$stickerBack.data);
	}
	<?php
		$stickerArea = array(
			array('$stickerFront','front'),
			array('$stickerSide','side'),
			array('$stickerRear','rear'),
			array('$stickerBack','back')
		);
	?>
	// SET SELECT STICKER STYLE
	function fn_selectSticker(style){
		$sticker.color = $stickerSlider.find('.slick-current .sticker-item').attr('data-color').split(',');
		// set default outline & preview sticker
		<?php foreach ($stickerArea as $k => $v) { ?>
		$('#popup-<?php echo $v[1]; ?>-crop').find('.sticker').attr('src',$sticker.path+$sticker.style+'-<?php echo $v[1]; ?>-'+$sticker.color[0]+'.png');
		$('.box-preview-color .<?php echo $v[1]; ?>').attr('src',$sticker.path+style+'-<?php echo $v[1]; ?>-'+$sticker.color[$sticker.colorSelect]+'.png');
		<?php } ?>

		// color
		$('.box-select-color').html('');
		for (var i = 0; i < $sticker.color.length ; i++) {
			$('.box-select-color').append('<a class="color-item" href="javascript:void(0);" onclick="fn_selectColor('+i+',this);"><img class="color" src="assets/img/sticker/color/'+$sticker.color[i]+'.png"></a>');
			$('.box-select-color .color-item').eq(0).addClass('active');
		}
	}
	// FN SLECT COLOR
	function fn_selectColor(color,_this){
		$sticker.colorSelect = color;
		$('.box-select-color .color-item.active').removeClass('active');
		$(_this).addClass('active');
		// sticker
		$('.box-preview-color .front').attr('src',$sticker.path+$sticker.style+'-front-'+$sticker.color[$sticker.colorSelect]+'.png');
		$('.box-preview-color .side').attr('src',$sticker.path+$sticker.style+'-side-'+$sticker.color[$sticker.colorSelect]+'.png');
		$('.box-preview-color .rear').attr('src',$sticker.path+$sticker.style+'-rear-'+$sticker.color[$sticker.colorSelect]+'.png');
		$('.box-preview-color .back').attr('src',$sticker.path+$sticker.style+'-back-'+$sticker.color[$sticker.colorSelect]+'.png');
		// color icon at result img
		$('.step-result .color-item').attr('src',"assets/img/sticker/color/"+$sticker.color[$sticker.colorSelect]+".png");
	}
	<?php foreach ($stickerArea as $k => $v) { ?>
	// STICKER
	var <?php echo $v[0]; ?> = {
		upload : null,
		data : null,
		url : null,
		init : function(){
			var flip = false;
			var orientation = 1;
			var zoom = 0;
			// set croppie
			<?php echo $v[0]; ?>.upload = $('#sticker-<?php echo $v[1]; ?>-crop').croppie({
				showZoomer: false,
				enableExif: true,
				enableOrientation: true,
				viewport: {width: '100%',height: '100%',type: 'square'},
				boundary: {width: '100%',height: '100%',}
			});
			// fn upload file
			$('#sticker-<?php echo $v[1]; ?>-upload').on('change', function () {
				<?php echo $v[0]; ?>.readFile(this);
			});
			// btn crop img
			$('#btn-crop-<?php echo $v[1]; ?>').click(function(e) {
				<?php echo $v[0]; ?>.upload.croppie('result', {
					type: 'canvas',
					size: { width: $sticker.sizeWidth, height: $sticker.sizeHeight },
					format: 'png'
				}).then(function (resp) {
					//console.log(resp);
					<?php echo $v[0]; ?>.data = resp;
					$('.<?php echo $v[1]; ?>-img-data').html('<img src="'+<?php echo $v[0]; ?>.data+'"/>');
					$.fancybox.close();
					//<?php echo $v[0]; ?>.fn_createImg(resp);
				});
			});
			// btn flip img
			$('#btn-flip-<?php echo $v[1]; ?>').click(function(){
				if(flip){
					flip = false;
					orientation = 1;
				} else {
					flip = true;
					orientation = 2;
				}
				<?php echo $v[0]; ?>.upload.croppie('bind',{
					url:<?php echo $v[0]; ?>.url,
			    orientation: orientation
				});
			});
			// btn zoom img
			$('.btn-zoom-<?php echo $v[1]; ?>').click(function(){
				var val = $(this).attr('data-zoom');
				if(val == 'in'){
					if(zoom < 1.5){
						zoom = zoom + 0.5;
					} else {
						zoom = 1.5;
					}
				} else {
					if(zoom != 0){
						zoom = zoom - 0.5;
					} else {
						zoom = 0;
					}
				}
				//console.log(zoom);
				<?php echo $v[0]; ?>.upload.croppie('setZoom', zoom);
			});
		},
		readFile : function(input){
			fn_loading();
			if(input.files && input.files[0]) {
				var reader = new FileReader();
				reader.onload = function (e) {
					<?php echo $v[0]; ?>.upload.croppie('bind', {
						url: e.target.result,
					}).then(function(){
						<?php echo $v[0]; ?>.upload.croppie('setZoom', 0);
						<?php echo $v[0]; ?>.url = e.target.result;
					});
					$.fancybox.open({
						src : '#popup-<?php echo $v[1]; ?>-crop',
						touch : false
					});
				}
				reader.readAsDataURL(input.files[0]);
				loading.close();
			} else {
				loading.close();
			}
		}
	}
	<?php } ?>

	// CREATE IMG
	function fn_createImg(dataFront,dataSide,dataRear,dataBack) {
		console.log($sticker);
		var cnv = document.getElementById("result-canvas");
		var ctx = cnv.getContext("2d");
		var image = {
			front : new Image(),
			side : new Image(),
			rear : new Image(),
			back : new Image()
		}
		var frame = {
			front : new Image(),
			side : new Image(),
			rear : new Image(),
			back : new Image()
		}
		var pos = {x:724,y:544}
		// FRONT
		image.front.src = dataFront;
		image.front.onload = function() {
			frame.front.src = $sticker.path + $sticker.style +'-front-'+$sticker.color[$sticker.colorSelect]+'.png';
			frame.front.onload = function() {
				//ctx.fillStyle = 'white';
				ctx.drawImage(image.front, 0, 0);
				ctx.drawImage(frame.front, 0, 0);
				// SIDE
				image.side.src = dataSide;
				image.side.onload = function() {
					frame.side.src = $sticker.path + $sticker.style +'-side-'+$sticker.color[$sticker.colorSelect]+'.png';
					frame.side.onload = function() {
						ctx.drawImage(image.side, pos.x, 0);
						ctx.drawImage(frame.side, pos.x, 0);
						// REAR
						image.rear.src = dataRear;
						image.rear.onload = function() {
							frame.rear.src = $sticker.path + $sticker.style +'-rear-'+$sticker.color[$sticker.colorSelect]+'.png';
							frame.rear.onload = function() {
								ctx.drawImage(image.rear, 0, pos.y);
								ctx.drawImage(frame.rear, 0, pos.y);
								// SIDE
								image.back.src = dataBack;
								image.back.onload = function() {
									frame.back.src = $sticker.path + $sticker.style +'-back-'+$sticker.color[$sticker.colorSelect]+'.png';
									frame.back.onload = function() {
										ctx.drawImage(image.back, pos.x, pos.y);
										ctx.drawImage(frame.back, pos.x, pos.y);
										// DRAW STROKE
										ctx.lineWidth = 2;
										ctx.strokeStyle = "#eb212e";
										ctx.strokeRect(1, 1, 718, 538);
										ctx.strokeRect(pos.x+1, 1, 718, 538);
										ctx.strokeRect(1, pos.y+1, 718, 538);
										ctx.strokeRect(pos.x+1, pos.y+1, 718, 538);
										/*ctx.fillRect(0, 0, $sticker.sizeWidth, $sticker.sizeHeight);
										ctx.drawImage(image.front, 720, 0, image.front.width, image.front.height, 720, 0, image.front.width, image.front.height);
										ctx.drawImage(image_frame, 720, 0, image_frame.width, image_frame.height, 720, 0, image_frame.width, image_frame.height);*/
										//ctx.restore();
										var image_url = cnv.toDataURL("image/png");
										//var link_download = image_url.replace(/^data:image\/[^;]/, 'data:application/octet-stream');
										$('#final-img').html('<img src="' + image_url + '"/>');

										// NEXT STEP
										$.fancybox.close();
										$(".box-step").hide();
										$("#step-4").show();
									}
								}
							}
						}
					}
				}
			};
		};
	}
	// LOADING
	var loading;
	function fn_loading(){
		loading = $.fancybox.open({
			src:'<div class="fancybox-loading"></div>',
			type:'html',
			touch: false,
			toolbar  : false,
			smallBtn : false,
			clickSlide: false,
			clickOutside: false,
			mobile : {
			  clickSlide: false,
			  clickOutside: false,
				touch: false,
			}
		});
	}
</script>
<?php include('inc/footer.php'); ?>