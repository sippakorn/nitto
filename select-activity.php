<?php include('inc/head.php'); ?>
	<?php include('inc/header.php'); ?>
	<main class="main-container" role="main">
		<section class="select-screen">
			<div class="container-fluid p-0">
				<div class="row no-gutters">
					<div class="col-12">
						<header class="header-page">
							<h1 class="head"><img src="assets/img/txt_select.png" alt="เลือกกิจกรรม"></h1>
						</header>
						<br><br>
						<a class="btn-select sRight" href="activity-1.php">
							<img class="bg" src="assets/img/btn_select-1-bg.png" alt="">
							<img class="txt" src="assets/img/btn_select-1-txt.png" alt="">
						</a>
						<br>
						<br>
						<a class="btn-select sLeft" href="#popup-activity-2" data-fancybox>
							<img class="bg" src="assets/img/btn_select-2-bg.png" alt="">
							<img class="txt" src="assets/img/btn_select-2-txt.png" alt="">
						</a>
					</div>
				</div>
			</div>
		</section>
		<div style="display:none;">
			<div class="popup popup-activity-2" id="popup-activity-2">
				<div class="head-popup">กิจกรรม 2</div>
				<div class="content-popup text-center">
					<div class="container-fluid">
						<div class="row">
							<div class="col-12">
								<div class="head-content"><h2 class="head mb-0"><strong>ลายนี้โคตรเจ๋ง<br>สำเพ็งไม่มีขาย</strong></h2></div>
								<div class="row justify-content-center">
									<div class="col-10">
										<p>
											xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
											xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
											xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
											xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
											xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
											xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
											xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
											xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
											xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
											xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
										</p>
										<br>
									</div>
								</div>
								<div class="row">
									<div class="col-6">
										<a href="#"><img src="assets/img/app-store.png" alt="App Store"></a>
									</div>
									<div class="col-6">
										<a href="#"><img src="assets/img/google-play.png" alt="Google Play"></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="popup popup-activity-2" id="popup-activity-2-complete">
				<div class="head-popup">กิจกรรม 2</div>
				<div class="content-popup text-center">
					<div class="container-fluid">
						<div class="row">
							<div class="col-12">
								<div class="head-content"><h2 class="head mb-0"><strong>ขอบคุณ<br><small>ที่ร่วมสนุกกับเรา</strong></small></h2></div>
								<div class="row justify-content-center">
									<div class="col-10">
										<p>
											xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
											xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
											xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
											xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
											xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
											xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
										</p>
										<br>
										<br>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>
<?php include('inc/javascript.php'); ?>
<?php include('inc/footer.php'); ?>